from nltk.tokenize import word_tokenize
import nltk
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import movie_reviews
from random import shuffle

class sentiment_analyser:
    def __init__(self,sentence):
        self.negids = movie_reviews.fileids('neg')
        self.posids = movie_reviews.fileids('pos')
        self.sentence = sentence
        self.features = self.getFeatures()
        self.all_words = self.getAllWords()
    def getFeatures(self):
        negfeats = [(self.wordFeatures(movie_reviews.words(fileids=[f])), 'neg') for f in self.negids]
        posfeats = [(self.wordFeatures(movie_reviews.words(fileids=[f])), 'pos') for f in self.posids]
        features = negfeats + posfeats
        shuffle(features)
        return features
    def getAllWords(self):
        all_words = []
        for feature in self.features:
            bag_of_words = feature[0].keys()
            for word in bag_of_words:
                all_words.append(word)
        return set(all_words)
    def wordFeatures(self,words):
        return dict([(word, True) for word in words])
    def organizeSentence(self):
        return {word.lower(): (word in word_tokenize(self.sentence.lower())) 
                              for word in self.all_words}
    def organizeClassifier(self):
        return nltk.NaiveBayesClassifier.train(self.features)
